/**
 *  Flynco - v1.0.0 - Main JavaScript file
 */


/* flynco Theme javascripts module */
var flynco = (function($) {

    'use strict';
    var _owlCarousel = function() {
            $(".loop").owlCarousel({
                center: true,
                loop: true,
                dots: true,
                autoPlay: 3000, //Set AutoPlay to 3 seconds
                responsive:{
                    600:{
                    items:2
                    }
                }
            });
        },
        _owlCarouselButton = function() {
            var owl = $('.loop').owlCarousel();
            $(".button-slider--prev").click(function () {
                owl.trigger('prev.owl.carousel');
            });
            $(".button-slider--next").click(function () {
                owl.trigger('next.owl.carousel');
            });
        },
        _mobileMenu = function() {
            $('.nav-toggle').click(function() {
            $('.main-nav__items').slideToggle();
            });
            // Hamburger to X toggle
            $('.nav-toggle').on('click', function() {
                this.classList.toggle('active');
            });
        },
        _fixedMenu = function() {
            $(window).bind('scroll', function() {
                if ($(window).scrollTop() > 148) {
                    $('.header-nav').addClass('fixed');
                }
                else {
                    $('.header-nav').removeClass('fixed');
                }
            });
        },
        _isotope = function() {
            $('.post-feed').isotope({
            // options
            itemSelector: '.post-feed__grid',
            masonry: {
                columnWidth: '.post-feed__grid',
                horizontalOrder: true
                }
            });
        },
        _isotopeFilter = function() {
            // external js: isotope.pkgd.js
            // init Isotope
            var $grid = $('.grid').isotope({
              itemSelector: '.element-item',
              layoutMode: 'fitRows',
              getSortData: {
                name: '.name',
                symbol: '.symbol',
                number: '.number parseInt',
                category: '[data-category]',
                weight: function( itemElem ) {
                  var weight = $( itemElem ).find('.weight').text();
                  return parseFloat( weight.replace( /[\(\)]/g, '') );
                }
              }
            });

            // filter functions
            var filterFns = {
              // show if number is greater than 50
              numberGreaterThan50: function() {
                var number = $(this).find('.number').text();
                return parseInt( number, 10 ) > 50;
              },
              // show if name ends with -ium
              ium: function() {
                var name = $(this).find('.name').text();
                return name.match( /ium$/ );
              }
            };

            // bind filter button click
            $('#filters').on( 'click', 'button', function() {
              var filterValue = $( this ).attr('data-filter');
              // use filterFn if matches value
              filterValue = filterFns[ filterValue ] || filterValue;
              $grid.isotope({ filter: filterValue });
            });

            // bind sort button click
            $('#sorts').on( 'click', 'button', function() {
              var sortByValue = $(this).attr('data-sort-by');
              $grid.isotope({ sortBy: sortByValue });
            });

            // change is-checked class on buttons
            $('.button-group').each( function( i, buttonGroup ) {
              var $buttonGroup = $( buttonGroup );
              $buttonGroup.on( 'click', 'button', function() {
                $buttonGroup.find('.is-checked').removeClass('is-checked');
                $( this ).addClass('is-checked');
              });
            });
        },
        _readingTime = function () {
            var $postArticleContent = $('.single-post__content');
            if ($postArticleContent.length) {
                $postArticleContent.readingTime({
                    wordCountTarget: $('.reading-time').find('.js-word-count')
                });
            }
        },
        _searchModule = function () {
            var openSearch = $('#search-field');
            var closeSearch = $('.close-search');
            var searchResult = $('.search-results');
            // ghost hunter init
            var ghostHunter = $('#search-field').ghostHunter({
                results: "#results",
                includepages: true,
                onKeyUp: true,
                info_template: "<h3 class='search-results__title'>Number of posts found: {{amount}}</3>",
                result_template: "<a href='{{link}}'><p><h4>{{title}}</h4><span>{{pubDate}}</span>{{description}}</p></a>"
            });
            openSearch.on('click', function (e) {
                e.preventDefault();
                searchResult.addClass('open');
            });
            closeSearch.on('click', function (e) {
                e.preventDefault();
                ghostHunter.clear();
                searchResult.removeClass('open');
            });
        },
        _fbPage = function () {
            if(config.fb_page == true){
                (function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.10";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));

                (function() {
                    var fb = document.createElement('div'); fb.id = 'fb-root';
                    (document.getElementsByClassName('site-wrapper')[0]).appendChild(fb);
                })();

                (function() {
                    $('.facebook-page').addClass('fb-active');
                })();
            } 
        },
       
        _instafeedJS = function () { 
            if(config.instagram_userId !== '' && config.instagram_userId !== null && config.instagram_userId !== undefined){
            
                var instagram_userId = config.instagram_userId;
                var instagram_accessToken = config.instagram_accessToken;

                (function() {
                    var userFeed = new Instafeed({
                    get: 'user',
                    limit: 12,
                    userId: instagram_userId,
                    accessToken: instagram_accessToken,
                    after: function() {
                        $('.owl-carousel').owlCarousel({
                            items:6,
                            loop:true,
                            dots: false,
                            margin:10,
                            autoplay:true,
                            autoplayTimeout:2000,
                            autoplayHoverPause:true
                        });
                    },
                    template: ' <div class="item"><a class="animation" href="{{link}}"><img src="{{image}}" /></a></div>',
                    });
                    userFeed.run();
                })(); 
            }
        }
      
    
    return {
        /* flynco theme initialization */
        init: function() {
            _owlCarousel();
            _owlCarouselButton();
            _mobileMenu();
            _fixedMenu();
            _isotope();
            _isotopeFilter();
            _readingTime();
            _searchModule();
            _fbPage();
            _instafeedJS();
           
         
        }
    };

})(jQuery);

/* flynco Theme javascripts initialization */
(function() {

	'use strict';
	flynco.init();

})();